import Vue from 'vue';
// other imports go here too

const template = require('./countdown.html');

const countdownComponent = Vue.component('vfz-countdown', {
    props: {
        // All component properties go here (see https://docs.gitlab.com/ee/development/fe_guide/style/vue.html#props)
    },
    data() {
        // data here
    },
    mounted() {
        // check lifecycle hooks for when/what to trigger https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram)
    },
    methods: {
        // methods
    },
    template,
});

export default countdownComponent;
